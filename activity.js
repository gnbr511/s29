// INSERT SAMPLE DATA
db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "09135685235",
			email: "steph.gmail.com"
		},
		courses: ["Python", "JavaScript", "Kotlin"],
		department: "HR"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "19345645134",
			email: "neil.gmail.com"
		},
		courses: ["PHP", "Laravel", "MySQL"],
		department: "HR"
	},
	{
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "09213456789",
			email: "jane.gmail.com"
		},
		courses: ["HTML", "CSS", "Bootstrap"],
		department: "HR"
	}
])

/*
2. Find users with letter s in their first name or d in their last name.
a. Use the $or operator.
b. Show only the firstName and lastName fields and hide the _id field.

*/
db.users.find(
	{
		$or: [
			{
				firstName: {
					$regex: "s",
					$options: "$i"
				}
			},
			{
				lastName: {
					$regex: "d",
					$options: "$i"
				}
			}
		]
	},
	{
		_id: 0,
		firstName: 1,
		lastName: 1
	}
)


/*
3. Find users who are from the HR department and their age is greater 
than or equal to 70.
a. Use the $and operator
*/

db.users.find({
	$and: [
		{
			department: "HR"
		},
		{
			age: { $gte: 70 }
		}
	]
})


/*
4. Find users with the letter e in their first name and has an age of less 
than or equal to 30.
a. Use the $and, $regex and $lte operators
*/

db.users.find({
	$and: [
		{
			firstName: {
				$regex: "e",
				$options: "$i"
			}
		},
		{
			age: { $lte: 30 }
		}
	]
})